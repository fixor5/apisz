SELECT *
FROM PARAMETER
WHERE PARAMETER IN ('UPLOAD_PATH_WEB','DOWNLOAD_FILE_URL','EXPORT_DIR','UPLOAD_FILE_PATH');

UPDATE PARAMETER
SET ertek = 'http://192.168.1.215:8080/ords/f?p=424:2' 		--beimport�lt app ID-ja kell ide
WHERE PARAMETER = 'UPLOAD_PATH_WEB';

/*
SELECT *
FROM PARAMETER
WHERE PARAMETER = 'DOWNLOAD_FILE_URL';
*/

INSERT INTO PARAMETER (
	PARAMETER,
	ertek,
	megjegyzes,
	modul,
	javithato_,
	nyomtat_,
	kotelezo_
)
VALUES (
	'DOWNLOAD_FILE_URL',
	'http://192.168.1.215:8080/ords/f?p=424:3',
	'Apexes f�jllet�lt�shez el�r�si �tvonal.',
	'AL',
	1,
	0,
	1
)

/*
UPDATE PARAMETER
SET ertek = 'http://192.168.1.215:8080/ords/f?p=424:3' 		--beimport�lt app ID-ja kell ide
WHERE PARAMETER = 'DOWNLOAD_FILE_URL';
*/

INSERT INTO PARAMETER (
	PARAMETER,
	ertek,
	megjegyzes,
	modul,
	javithato_,
	nyomtat_,
	kotelezo_
)
VALUES (
	'EXPORT_DIR',
	'/mnt/ik_file_test/',
	'Az oracle szerveren l�tez� mappa',
	'AL',
	1,
	0,
	1
)

/*
UPDATE PARAMETER
SET ertek = '/mnt/ik_file_test/' 	
WHERE PARAMETER = 'EXPORT_DIR';
*/

UPDATE PARAMETER
SET ertek = '/mnt/ik_file_test'	
WHERE PARAMETER = 'UPLOAD_FILE_PATH';

UPDATE IK_DIR
SET ias_dir = '/u02/szalay_test/ik_file_test'
WHERE ik_dir_id = 2;


SELECT *
FROM SM_PARAMETER